(function () {
  'use strict';
  if ((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPad/i))) {
    document
      .querySelector('html')
      .classList
      .add('is-ios');
  }
  var body = $('.body');

  var header = $('#js-header');
  var burger = $('#js-burger');
  var shadow = $('#js-shadow');
  burger.on('click', function () {
    $(this).toggleClass('page-header__burger--active');
    header.toggleClass('page-header--active');
    shadow.toggleClass('shadow--showed');
    body.toggleClass('overflow');
  });

  var searchInput = $('#js-searchInput');
  var searchBtn = $('.js-searchBtn');
  searchBtn.on('click', function () {
    searchInput.toggleClass('page-header__search-wrap--active');
    header.removeClass('page-header--active');
    body.toggleClass('overflow');
    shadow.addClass('shadow--showed');
    body.addClass('overflow');
  });

  shadow.on('click', function () {
    $(this).removeClass('shadow--showed');
    burger.removeClass('page-header__burger--active');
    header.removeClass('page-header--active');
    searchInput.removeClass('page-header__search-wrap--active');
    body.removeClass('overflow-tablet');
    body.removeClass('overflow');
  });
})();