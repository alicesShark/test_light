# test_light

## Build Setup

``` bash
# install dependencies
$ npm install

# build for production and launch server
$ gulp build

# serve with hot reload at localhost:3000
$ gulp serv

```
